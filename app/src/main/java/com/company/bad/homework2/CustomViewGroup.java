package com.company.bad.homework2;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class CustomViewGroup extends ViewGroup {

    int deviceWidth;

    public CustomViewGroup(Context context) {
        this(context,null,0);
    }

    public CustomViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public CustomViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point deviceDisplay = new Point();
        display.getSize(deviceDisplay);
        deviceWidth = deviceDisplay.x;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        final int count = getChildCount();
        int curWidth, curHeight, curLeft, curTop, maxHeight;

        final int childLeft = this.getPaddingLeft();
        final int childTop = this.getPaddingTop();
        final int childRight = this.getMeasuredWidth() - this.getPaddingRight();
        final int childBottom = this.getMeasuredHeight() - this.getPaddingBottom();
        final int childWidth = childRight - childLeft;
        final int childHeight = childBottom - childTop;

        maxHeight = 0;
        curLeft = childRight;
        curTop = childTop;

        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);

            if (child.getVisibility() == GONE)
                continue;

            child.measure(MeasureSpec.makeMeasureSpec(childWidth, MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(childHeight, MeasureSpec.AT_MOST));
            curWidth = child.getMeasuredWidth();
            curHeight = child.getMeasuredHeight();

            int j;
            View oldChild;
            int oldLeft = curLeft - curWidth;
            for (j = i - 1; j >= 0 && getChildAt(j).getY() == curTop; j--)
                oldLeft -= getChildAt(j).getMeasuredWidth();
            if (oldLeft >= 0){
                j = i - 1;
                oldLeft = curLeft - curWidth;
            } else {
                curTop += maxHeight;
                maxHeight = 0;
            }

            while (j >= 0 && (oldChild = getChildAt(j)).getY() == curTop){
                j--;
                oldLeft -= oldChild.getMeasuredWidth();
                if (oldLeft < 0){
                    break;
                }
                oldChild.layout(oldLeft, curTop, oldLeft + oldChild.getMeasuredWidth(), curTop + curHeight);
            }

            child.layout(curLeft - curWidth, curTop, curLeft, curTop + curHeight);
            if (maxHeight < curHeight)
                maxHeight = curHeight;
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int count = getChildCount();
        int maxHeight = 0;
        int childState = 0;
        int mLeftWidth = 0;

        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);

            if (child.getVisibility() == GONE)
                continue;

            measureChild(child, widthMeasureSpec, heightMeasureSpec);

            if (mLeftWidth + child.getMeasuredWidth() > deviceWidth) {
                mLeftWidth = child.getMeasuredWidth();
                maxHeight += child.getMeasuredHeight();
            } else {
                mLeftWidth += child.getMeasuredWidth();
                maxHeight = Math.max(maxHeight, child.getMeasuredHeight());
            }
            childState = combineMeasuredStates(childState, child.getMeasuredState());
        }

        maxHeight = Math.max(maxHeight, getSuggestedMinimumHeight());

        setMeasuredDimension(resolveSizeAndState(deviceWidth, widthMeasureSpec, childState),
                resolveSizeAndState(maxHeight, heightMeasureSpec, childState << MEASURED_HEIGHT_STATE_SHIFT));
    }
}
