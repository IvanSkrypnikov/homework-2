package com.company.bad.homework2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.robertlevonyan.views.chip.Chip;
import com.robertlevonyan.views.chip.OnCloseClickListener;

public class MainActivity extends AppCompatActivity {



    private static final String[] firstArray = new String[]{"Арбатская","Таганская","Текстильщики","Серпуховская","Театральная","Римская","Университет","Сокольники","Красносельская","Комсомольская"};
    private static final String[] secondArray = new String[]{"Охотный ряд","Курская","Китай-город","ВДНХ","Киевская","Перово","Технопарк","Речной вокзал","Водный стадион","Войковская"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CustomViewGroup firstCustomViewGroup = (CustomViewGroup) findViewById(R.id.firstViewGroup);
        CustomViewGroup secondCustomViewGroup = (CustomViewGroup) findViewById(R.id.secondViewGroup);

        setChipValue(firstCustomViewGroup,secondCustomViewGroup,firstArray);
        setChipValue(secondCustomViewGroup,firstCustomViewGroup,secondArray);

    }

    private void setChipValue(CustomViewGroup customViewGroup, CustomViewGroup toAddCustomViewGroup, String[] values){
        LayoutInflater layoutInflater = getLayoutInflater();
        for (String value: values) {
            View groupView = layoutInflater.inflate(R.layout.child_view, null, false);
            Chip chip = (Chip) groupView.findViewById(R.id.chip);
            chip.setChipText(value);
            setListener(chip,customViewGroup,toAddCustomViewGroup);
            customViewGroup.addView(groupView);
        }
    }

    private void setListener(final Chip chip, final CustomViewGroup firstViewGroup, final CustomViewGroup secondViewGroup){
        chip.setOnCloseClickListener(new OnCloseClickListener() {
            @Override
            public void onCloseClick(View v) {
                LinearLayout parent = (LinearLayout) chip.getParent();
                for (int i = 0; i < firstViewGroup.getChildCount(); i++){
                    if (firstViewGroup.getChildAt(i) == parent){
                        firstViewGroup.removeView(parent);
                        secondViewGroup.addView(parent);
                    }
                    if (firstViewGroup.getChildAt(i) == parent){
                        secondViewGroup.removeView(parent);
                        firstViewGroup.addView(parent);
                    }
                }
                setListener(chip,secondViewGroup,firstViewGroup);
            }
        });
    }
}
